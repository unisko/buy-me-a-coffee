# books.xfoss.com

## 书籍

- [最新：Rust 编程语言 ®️](https://rust.xfoss.com/)
- [学习 Java 编程语言 ☕️](https://java.xfoss.com/)
- [学习 TypeScript 编程语言 🎓](https://ts.xfoss.com/)
- [60 天通过 CCNA 考试（计算机网络） 🖧](https://ccna60d.xfoss.com/)


<details>
    <summary>打赏（donate）💰</summary>

![支付宝-Alipay](alipay-laxers.png)

*支付宝-Alipay，扫码付款*

![微信支付-WeChat Pay](wechat-pay-lenny.png)

*微信支付-WeChat Pay, 扫码付款*


![比特币付款-Bitcoin](btc-qrcode.png)

*比特币-Bitcoin，扫码付款*

</details>
